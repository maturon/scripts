# Scripts

This repo holds various scripts and wrappers I've been working on or use semi-regularly

## Contents

- *Uploadr*: A wrapper for the python3 http.server module. Adds a list of uploadable files on the command line, lists available IPs from which to upload files, and allows for flags to specify launch directory and port number.
- *Wifi-Toggle*: A quick and dirty internet connection (needed to use this constantly when my eth card died).
- *XLoggr*: Keylogging program that leverages xinput to monitor X11 input socket.

## Licenses

[GNU GPLv.3](https://choosealicense.com/licenses/gpl-3.0/)
