# Wifi-Toggle

A quick and dirty internet connection (needed to use this constantly when my eth card died).

Requires:
- `wpa_supplicant`
- `dhcpcd`
- root priv

### Usage

Initial connection:
```bash
sudo ./wifi-toggle

No wpa_supplicant entry for device wlp8s0. Creating one now.
SSID: MyRouterName
Password: ***********

Generating /etc/wpa_supplicant/wlp8s0...
Connecting...
```