# Eth-Toggle

A quick and dirty ethernet connection

Requires:
- `dhcpcd`
- root priv

### Usage

Initial connection:
```bash
sudo ./eth-toggle
```