# Uploadr

A wrapper for the python3 http.server module. Adds a list of uploadable files on the command line, lists available IPs from which to upload files, and allows for flags to specify launch directory and port number

### Usage

Without flags:
```bash
./uploadr
```

With flags:
```bash
./uploadr -l ~/directory/to/launch/from -p 8080
````