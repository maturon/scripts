#!/bin/bash

# Vars
port=8888
loc='.'
max_port=65535
is_num="^[0-9]+$"

# Colorize
C=$(printf '\033')
W="${C}[1;37m"
R="${C}[1;31m"
B="${C}[1;34m"
G="${C}[1;32m"
T="${C}[1;36m"
Y="${C}[1;33m"
RST="${C}[1;0m"

print_usage() {
  echo "Usage:"
  echo ""
  echo "$0 [ -l LOCATION ] [ -p PORT ]" 1>&2
  echo "Specified port must be a valid port number (1-$max_port)"
  echo "Default port is 8888"
  exit 1
}

while getopts ":l:p:" options; do
  case "${options}" in
    l)  loc="${OPTARG}" 
        if [ ! -d $loc ]; then
          print_usage
        fi
        ;;
    p)  port="${OPTARG}"
        if [[ ! ($port =~ $is_num) || $port -gt $max_port || $port -lt 1 ]]; then
          print_usage
        fi
        ;;
    *)  print_usage
        ;;
  esac
done

print_banner() {
  echo "$R"
  echo "  ▄• ▄▌ ▄▄▄·▄▄▌         ▄▄▄· ·▄▄▄▄  ▄▄▄  "
  echo "  █▪██▌▐█ ▄███•  ▪     ▐█ ▀█ ██▪ ██ ▀▄ █·"
  echo "  █▌▐█▌ ██▀·██▪   ▄█▀▄ ▄█▀▀█ ▐█· ▐█▌▐▀▀▄ "
  echo "  ▐█▄█▌▐█▪·•▐█▌▐▌▐█▌.▐▌▐█ ▪▐▌██. ██▌▐█•█▌"
  echo "   ▀▀▀ .▀   .▀▀▀  ▀█▄▀▪ ▀  ▀ ▀▀▀▀▀• .▀  ▀"
  echo "$W A simple wrapper for python's http.server module"
  echo "                                       by$R Maturon$Y"
}

print_files() {
  cd $loc
  echo "FILE LIST:"
  echo "$W---------"
  lines=$(find . -maxdepth 4 -type f -o -type l)
  for line in $lines; do

    # Reverse string, truncate everything beyond first '/', then reverse back to store directory path, then remove '.'
    dirpath=$(ls -Ad $line | rev | sed -e 's/^[^/]*\///' | rev)
    dirpath=$(echo $dirpath | sed -e 's/^\.//')

    # Reverse string, cut everything after first '/' to store filename
    filename=$(ls -Ad $line | rev | cut -d/ -f1 | rev)

    echo -n "$B$dirpath/"

    if [ -L $line ]; then
      echo "$T$filename -> $(ls -l $line | tr -s ' ' | cut -d' ' -f11)"
    elif [ -x $line ]; then
      echo "$G$filename"
    else
      echo "$W$filename"
    fi
  done
}

run_http_server() {
  # Output listening address(es) and port(s)
  echo "$Y"
  echo "LISTENING ON:"
  echo "$W------------"

  # Parse all non-loopback IP addresses from 'ip addr' command
  addrs=$(ip addr | grep 'inet ' | grep -v '127.0.0.' | cut -d' ' -f6 | cut -d/ -f1)
  for addr in $addrs; do
    echo "$G + $Y$addr:$G$port"

    # Generate 'quick copy' list
    echo "    $W DL to remote host: $Y wget $addr:$port$B[FILE PATH]"
    echo "    $W Run without DL:    $Y curl -sL $addr:$port$B[FILE PATH]$Y | sh$W"
  done
  echo

  # Run our http server
  python3 -m http.server $port
}

print_banner
print_files
run_http_server
exit 0
